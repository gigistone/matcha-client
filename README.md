# Matcha
Dating web app
Built in reactjs (backend in node).

This repo only contains the front-end application.


# Pics

<p align="center">
  <img src="https://user-images.githubusercontent.com/44972661/66797691-f3794300-ef0b-11e9-8e0f-5e6d4070ae9b.PNG">
  <img src="https://user-images.githubusercontent.com/44972661/66797692-f3794300-ef0b-11e9-8380-867bb4969ab9.PNG">
  <img src="https://user-images.githubusercontent.com/44972661/66797693-f3794300-ef0b-11e9-9a6c-571d262b09c8.PNG">
  <img src="https://user-images.githubusercontent.com/44972661/66797694-f411d980-ef0b-11e9-9365-f64f15a3072c.PNG">
</p>

# Feature
Account and user management  
  
Validation email and forget password  
  
Geolocation by browser or ip  
  
Selection of the proposed profiles according to romantic affinities and location  
  
Profile search by location or other parameters: age tags ...  
  
Visible scoring system  
  
Match system  
  
Live chat available after a match  
  
live notification  
  
History of people who viewed your profile  
  
History of people who liked your profile  
  
Setting panel  
  
Love preferences  
  
Modifiable personal data: Gender, Name, Email ...  
  
Tag management  
  
Photo upload and Profile photo system  
  
Generating a seed of false accounts to feed the application  
  
Seed random.  
  
Profile images are made in random procedural generation in 8bit style


# How to

### In the root folder:
npm install && npm start 



# State
This project is archived
